import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SkiingInSingapore {

    private static int width = 0, height = 0;
    private static List<List<Integer>> data = new ArrayList<>();

    public static void main(String[] args) {
        boolean firstLine = true;
        try {
            for (String line : Files.readAllLines(Paths.get("big_map.txt"))) {
                if (firstLine) {
                    width = Integer.parseInt(line.split(" ")[0]);
                    height = Integer.parseInt(line.split(" ")[1]);
                    firstLine = false;
                } else {
                    List<Integer> row = new ArrayList<>();
                    for (String num : line.split(" ")) {
                        row.add(Integer.parseInt(num));
                    }
                    data.add(row);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Integer> result = solve();
        System.out.println("Longest path: " + result.get(0));
        System.out.println("Drop: " + (result.get(1) - result.get(2)));
    }

    /**
     *  Method to run the overall solve:
     *      findRoute for each possible starting point, collect best routes (length, steepest drop), return best option
      */
    private static List<Integer> solve() {
        List<List<Integer>> options = new ArrayList<>();
        for (int row = 0; row < width; row++) {
            for (int col = 0; col < height; col++) {
                options.add(findRoute(row, col, 1, valueAt(row, col), valueAt(row, col)));
            }
        }
        return bestOption(options);
    }

    /**
     *  Recurring method to find and explore possible routes:
     *      checks if you can ski north/east/south/west and explores those possibilities, returning the longest run
     *      with the steepest drop
     */
    private static List<Integer> findRoute(int row, int col, int len, int startingVal, int currentVal) {
        List<List<Integer>> options = new ArrayList<>();

        // If you can ski north
        if (row > 0) {
            options.add(ski(row - 1, col, len, startingVal, currentVal));
        }

        // If you can ski south
        if (row < height - 1) {
            options.add(ski(row + 1, col, len, startingVal, currentVal));
        }

        // If you can ski west
        if (col < width - 1) {
            options.add(ski(row, col + 1, len, startingVal, currentVal));
        }

        // If you can ski east
        if (col > 0) {
            options.add(ski(row, col - 1, len, startingVal, currentVal));
        }

        return bestOption(options);
    }

    /**
     *  Method to check the elevation of the new box is lower -
     *      if it is:
     *          check if steepestDrop needs to be updated,
     *          continue exploring routes from the new box and return the best option from that box
     *      if it isn't
     *          return the current length, startingVal, and currentVal (val in last box on route)
     */
    private static List<Integer> ski(int newRow, int newCol, int len, int startingVal, int currentVal) {
        int nextVal = valueAt(newRow, newCol);
        if (nextVal < currentVal) {
            return findRoute(newRow, newCol, len + 1, startingVal, nextVal);
        }
        List<Integer> result = new ArrayList<>();
        result.add(len);
        result.add(startingVal);
        result.add(currentVal);
        return result;
    }

    /**
     *  Cleaner access of values
     */
    private static int valueAt(int row, int col) {
        return data.get(row).get(col);
    }

    /**
     *  Find and return the longest option with the steepest drop from options
     */
    private static List<Integer> bestOption(List<List<Integer>> options) {
        List<Integer> winningOption = options.get(0);
        for (List<Integer> option : options) {
            // If the option's length is larger than the current winning option - or if option len == winningOption len then decide based on steepest drop
            if (option.get(0) > winningOption.get(0)) {
                winningOption = option;
            } else if (option.get(0).equals(winningOption.get(0)) && option.get(1) - option.get(2) > winningOption.get(1) - winningOption.get(2)) {
                winningOption = option;
            }
        }
        return winningOption;
    }
}
